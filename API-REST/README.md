### Créer un Environnement virtuel

```SHELL
python3 -m venv environnement_virtuel
```

### Architecture générale du projet

Le projet comprendra :

Une couche de présentation (ROUTER) où les routes d'accès au format HTTP seront définies.
Une couche de services (CORE) qui implémentera les contraintes métiers.
Une couche de persistance (DB) responsable de la sauvegarde des données.

Nous aurons également besoin des bibliothèques suivantes :
- pip install fastapi
- pip install uvicorn 
- pip install python-jose[cryptography]
- pip install passlib[bcrypt]
- pip install python-multipart
- pip install python-dotenv


