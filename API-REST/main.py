# main.py

import logging.config
from fastapi import FastAPI
from app.router.router_base import api_router_base
import uvicorn
import os

# Configurer le journal
log_directory = os.path.join(os.path.dirname(__file__), 'app', 'config', 'logging_config.ini')
logging.config.fileConfig(log_directory)
logger = logging.getLogger('apiLogger')

app = FastAPI()

@app.get("/")
async def root():
    """Route racine de l'API."""
    return {"message": "Hello World"}

# Inclure le routeur de base
app.include_router(api_router_base)

if __name__ == "__main__":
    uvicorn.run("main:app", port=8080, reload=True, access_log=True)