# app/router/router_login.py

from fastapi import APIRouter, Depends, status, HTTPException
from fastapi.security import OAuth2PasswordRequestForm
from typing import Dict
from app.models.model_token import Token
from app.dependencies.dependencies import get_user_repository, get_security
from app.core.security import Security
from app.db.user_repository import UserRepository
import logging

# Création du routeur pour les opérations liées à l'authentification
router = APIRouter()
logger = logging.getLogger('apiLogger')
user_repository: UserRepository = get_user_repository()
security: Security = get_security()

# Définition de la route de connexion
@router.post("/login", response_model=Token)
async def login_for_access_token(
    form_data: OAuth2PasswordRequestForm = Depends(),
) -> Dict[str, str]:

    try:
        username = form_data.username.strip().lower()
        user = user_repository.get_user_db(username)

        if not user:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail='Invalid Credentials',
            )

        if not security.verify_password(form_data.password, user.hashed_password):
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail='Invalid Password',
            )
        
        if user.is_active:
            access_token = security.create_access_token(user.email, user.roles)
            return {"access_token": access_token, "token_type": "bearer"}
        
        else:
            logger.error(f"Votre compte est désactivé")


    except Exception as generic_exception:
        logger.error(f"Une erreur non gérée s'est produite : {generic_exception}")
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="Internal Server Error",
        ) from generic_exception

