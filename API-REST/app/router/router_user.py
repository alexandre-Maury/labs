# app/router/router_user.py

from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from app.dependencies.dependencies import get_user_repository, get_security
from app.models.model_user import ShowUser, UserCreate
from app.models.model_token import TokenData
from app.db.user_repository import UserRepository
from app.core.security import Security
from typing import List
import logging

# Création du routeur pour les opérations liées aux utilisateurs
router = APIRouter()
logger = logging.getLogger('apiLogger')
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")
user_repository: UserRepository = get_user_repository()
security: Security = get_security()

# Définition des opérations liées aux utilisateurs

@router.post("/register", response_model=ShowUser, status_code=status.HTTP_201_CREATED)
def create_user(user: UserCreate) -> UserCreate :

    try:
        user.roles = user.roles or ['USER']
        user.hashed_password = security.get_password_hash(user.plain_password)
        user.plain_password = None
        created_user = user_repository.create_user_db(user)

        return created_user

    except ValueError as e:
        logger.error(f"Une erreur s'est produite lors de la création de l'utilisateur : {e}")
        raise


@router.get('/protected_route/get_user/{email}', response_model=ShowUser)
async def get_user_protected_route(
    email: str,
    token: str = Depends(oauth2_scheme)) -> UserCreate:

    try:
        credentials_exception = HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )

        token: TokenData = security.verify_token(token, credentials_exception)

        if email != token.email :
            raise HTTPException(status_code=403, detail="Data is private")

        user: UserCreate = user_repository.get_user_db(token.email)
        return user
    
    except HTTPException as e:
        logger.error(f"Erreur lors de la récupération des détails de l'utilisateur : {e}")
        raise
    except Exception as e:
        logger.error(f"Erreur non gérée lors de la récupération des détails de l'utilisateur : {e}")
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="Internal Server Error",
        ) from e
    

@router.put("/protected_route/update_user/{email}", response_model=ShowUser)
async def update_protected_route(user: UserCreate = Depends(get_user_protected_route)) -> None:
    try:
        user.description = "Mise a jours de l'utilisateur"
        user.roles.append("ADMINISTRATEUR")
        user_repository.save_user_db(user)
        return user

    except Exception as e:
        logger.error(f"Une erreur est survenue lors de la sauvegarde de l'utilisateur : {e}")


@router.get('/protected_route/get_all_user/{email}', response_model=List[ShowUser])
async def get_all_user_protected_route(
    email: str,
    token: str = Depends(oauth2_scheme)) -> List[UserCreate]:

    try:
        credentials_exception = HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )

        token: TokenData = security.verify_token(token, credentials_exception)

        if email != token.email :
            raise HTTPException(status_code=403, detail="Data is private")
        
        if token.roles != ["ADMINISTRATEUR"] :
            raise HTTPException(status_code=403, detail="Data is private")

        user: UserCreate = user_repository.get_all_user_db()

        return user
    
    except HTTPException as e:
        logger.error(f"Erreur lors de la récupération des détails de l'utilisateur : {e}")
        raise
    except Exception as e:
        logger.error(f"Erreur non gérée lors de la récupération des détails de l'utilisateur : {e}")
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="Internal Server Error",
        ) from e





    
