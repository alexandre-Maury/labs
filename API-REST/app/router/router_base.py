# app/router/router_base.py

from fastapi import APIRouter
from app.router.router_user import router as user_router
from app.router.router_login import router as login_router

# Création du routeur de base
api_router_base = APIRouter()
api_router_base.include_router(user_router, prefix="/users", tags=["users"])
api_router_base.include_router(login_router, prefix="/auth", tags=["login"])
