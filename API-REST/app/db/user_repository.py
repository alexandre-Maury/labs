# app/db/user_repository.py

from app.models.model_user import UserCreate
from fastapi import Response
from typing import List
import logging

class UserRepository:

    def __init__(self, base_repository) -> None:
        self.base_repository = base_repository
        self.logger = logging.getLogger('apiLogger')

    """Classe pour la gestion des utilisateurs."""

    def create_user_db(self, user: UserCreate) -> UserCreate:

        """Créer un utilisateur dans la base de données."""

        try:
            if any(existing_user.email == user.email for existing_user in self.base_repository.users):
                self.logger.warning(f"Un utilisateur avec cette adresse e-mail : {user.email} existe déjà.")
            else:
                self.base_repository.users.append(user)
                self.base_repository._dump_data()
                self.logger.info(f"Création de l'utilisateur : {user.email} avec succés")


        except Exception as e:
            self.logger.error(f"Une erreur est survenue lors de la création de l'utilisateur : {e}")

        return user

    def get_user_db(self, user_email: str) -> UserCreate:

        """Obtenir un utilisateur de la base de données."""

        try:
            return next((data for data in self.base_repository.users if data.email == user_email), None)
        except Exception as e:
            self.logger.error(f"Une erreur est survenue lors de la recherche de l'utilisateur : {e}")

    def get_all_user_db(self) -> List[UserCreate]:

        """Obtenir tous les utilisateurs de la base de données."""

        try:
            users = []

            for user in self.base_repository.users:
                users.append(user)

            return users

        except Exception as e:
            self.logger.error(f"Une erreur est survenue lors de la recherche de l'utilisateur : {e}")
            raise


    def save_user_db(self, updated_user: UserCreate) -> None:

        """Sauvegarder les modifications d'un utilisateur dans la base de données."""

        try:
            # Recherchez l'index de l'utilisateur dans la liste
            user_index = next((index for index, user in enumerate(self.base_repository.users) if user.email == updated_user.email), None)

            if user_index is not None:
                # Remplacez l'utilisateur existant par le nouvel utilisateur mis à jour
                self.base_repository.users[user_index] = updated_user
                # Enregistrez les modifications dans le fichier
                self.base_repository._dump_data()
                self.logger.info(f"Modification de l'utilisateur : {updated_user.email} avec succés")
            else:
                self.logger.warning(f"L'utilisateur avec l'adresse e-mail {updated_user.email} n'a pas été trouvé.")
        except Exception as e:
            self.logger.error(f"Une erreur est survenue lors de la sauvegarde de l'utilisateur : {e}")

