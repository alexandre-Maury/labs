# app/db/base_repository.py

from app.models.model_user import UserCreate
from typing import List
import json
import logging
import os

# Définition de la classe BaseRepository

class BaseRepository:

    def __init__(self, filename) -> None:
        self.script_dir = os.path.dirname(__file__)
        self.data_file = os.path.join(self.script_dir, filename)
        self.logger = logging.getLogger('apiLogger')
        self.users = self._load_data()

    def _load_data(self) -> List[UserCreate]:
        """Charger les données depuis le fichier."""
        if not os.path.exists(self.data_file):
            self.logger.error(f"Le fichier {self.data_file} n'existe pas.")
            return []

        try:
            with open(self.data_file, "r", encoding="utf-8") as file:
                data = json.load(file)
        except (FileNotFoundError, json.JSONDecodeError) as e:
            self.logger.error(f"Une erreur s'est produite lors du chargement des données : {e}")
            return []

        if not data:
            self.logger.warning("Le fichier de données est vide.")
            return []

        user_list = [UserCreate(**user_data) for user_data in data]
        return user_list

    def _dump_data(self) -> None:
        """Sauvegarder les données dans le fichier."""
        try:
            serialized_users = [user.model_dump() for user in self.users]

            with open(self.data_file, "w", encoding="utf-8") as file:
                json.dump(serialized_users, file, indent=2)

        except IOError as e:
            self.logger.error(f"Erreur lors de la sauvegarde des données : {e}")


