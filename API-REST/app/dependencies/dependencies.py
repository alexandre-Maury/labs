# app/dependencies/dependencies.py

from app.db.base_repository import BaseRepository
from app.db.user_repository import UserRepository
from app.core.security import Security


FILENAME_REPO = "users_data.json"

# Définition des dépendances

def get_base_repository() -> BaseRepository:
    """Obtenir une instance de base_repository."""
    return BaseRepository(FILENAME_REPO)

def get_user_repository(base_repository: BaseRepository = get_base_repository()) -> UserRepository:
    """Obtenir une instance de user_repository."""
    return UserRepository(base_repository)

def get_security() -> Security:
    """Obtenir une instance de la classe Security."""
    return Security()
