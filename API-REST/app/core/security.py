# app/core/security.py

from app.models.model_token import TokenData
from fastapi import HTTPException, status
from datetime import datetime, timedelta
from passlib.context import CryptContext
from passlib.exc import PasslibWarning
from dotenv import dotenv_values
from jose import JWTError, jwt
import logging
import os

class Security:

    """Classe pour la gestion de la sécurité."""

    def __init__(self) -> None:
        self.pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
        self.logger = logging.getLogger('apiLogger')
        self.config = dotenv_values(os.path.join(os.path.dirname(__file__), '../config/.env'))
        self.SECRET_KEY: str = self.config.get('SECRET_KEY', 'fallback_secret_key')
        self.ALGORITHM: str = self.config.get('ALGORITHM', 'fallback_secret_key')
        self.ACCESS_TOKEN_EXPIRE_MINUTES: int = self.config.get('ACCESS_TOKEN_EXPIRE_MINUTES', 'fallback_secret_key')


    def verify_password(self, plain_password, hashed_password):

        """Vérifier le mot de passe."""

        try:
            return self.pwd_context.verify(plain_password, hashed_password)
        except PasslibWarning as e:
            self.logger.error(f"Erreur lors de la vérification du mot de passe : {e}")
            raise

    def get_password_hash(self, password):

        """Obtenir le hachage du mot de passe."""

        try:
            return self.pwd_context.hash(password)
        except PasslibWarning as e:
            self.logger.error(f"Erreur lors de la génération du hachage du mot de passe : {e}")
            raise

    def create_access_token(self, user_email: str, user_roles: list):

        """Créer un jeton d'accès."""

        try:
            expire = datetime.utcnow() + timedelta(minutes=self.ACCESS_TOKEN_EXPIRE_MINUTES)
            to_encode = {"sub": user_email, "exp": expire, "role": user_roles}
            encoded_jwt = jwt.encode(to_encode, self.SECRET_KEY, algorithm=self.ALGORITHM)
            return encoded_jwt
        except Exception as e:
            self.logger.error(f"Erreur lors de la création du jeton d'accès : {e}")
            raise

    def verify_token(self, token: str, credentials_exception) -> TokenData:

        """Vérifier le jeton d'accès."""

        try:
            payload = jwt.decode(token, self.SECRET_KEY, algorithms=[self.ALGORITHM])

            tokenData: TokenData = TokenData
            tokenData.email = payload.get("sub")
            tokenData.roles = payload.get("role")
            tokenData.expire = payload.get("expire")

            if tokenData.email is None or tokenData.roles is None:
                raise credentials_exception

            return tokenData

        except JWTError as e:
            self.logger.error(f"Erreur lors de la vérification du jeton d'accès : {e}")
            raise credentials_exception
