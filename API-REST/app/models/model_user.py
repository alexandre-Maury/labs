# app/models_user.py

from pydantic import BaseModel, EmailStr
from typing import Optional
import uuid

# Définition des modèles liés aux utilisateurs
class UserCreate(BaseModel):
    """Modèle de création d'utilisateur."""
    id: str = str(uuid.uuid4())
    email: EmailStr
    plain_password: Optional[str] = None
    hashed_password: Optional[str] = None
    roles: Optional[list] = None
    description: Optional[str] = None
    is_active: Optional[bool] = True


class ShowUser(BaseModel):
    """Modèle de présentation d'utilisateur."""
    id: str
    email: EmailStr
    roles: list
    description: Optional[str]


class ShowAllUser(BaseModel):
    """Modèle de présentation de tous les utilisateurs."""
    id: str
    email: EmailStr
    roles: list
    description: Optional[str]
    is_active: Optional[bool]
