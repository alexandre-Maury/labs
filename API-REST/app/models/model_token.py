# app/models_token.py

from typing import Optional
from pydantic import BaseModel

# Définition des modèles liés aux jetons
class Token(BaseModel):
    """Modèle de jeton."""
    access_token: str
    token_type: str


class TokenData(BaseModel):
    """Modèle de données du jeton."""
    email: Optional[str] = None
    roles: Optional[list] = None
    expire: Optional[str] = None

